const express= require("express")
const app = express()
const dotenv = require("dotenv")
const mongoose = require("mongoose")

const userRoute = require("./Routes/userRoutes.js")
const authRoute = require("./Routes/auth.js")
const productRoute = require("./Routes/productRoutes.js")
const cartRoute = require("./Routes/cartRoutes.js")
const orderRoute = require("./Routes/orderRoutes")

dotenv.config()

mongoose.connect(process.env.mongo_url)
.then(() => console.log("Database Connection Success"))
.catch(() => {
    console.log(err);
})

// for postman to accept json inputs : 
app.use(express.json())

app.use("/api/auth", authRoute)
app.use("/api/users", userRoute)
app.use("/api/products", productRoute)
app.use("/api/carts", cartRoute)
app.use("/api/orders", orderRoute)




app.listen(process.env.port || 5000, () => {
    console.log("Backend server is runnning!")
})


