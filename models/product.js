const mongoose = require("mongoose")

const ProductSchema = new mongoose.Schema(
    {
        title:{
            type : String,
        
        },
        desc:{
            type : String,
  
        },
        categories:{
            type : Array
        },
        size:{
            type : String,
        },
        isActive:{
            type : Boolean,
        },
        price:{
            type : Number,
            unique : false
        }

    },
    { timestamps : true}
)

module.exports = mongoose.model("Product", ProductSchema)