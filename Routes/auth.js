const router = require("express").Router()
const User = require("../models/user")
const CryptoJS = require("crypto-js")
const jwt = require("jsonwebtoken")
// const dotenv = require("dotenv")

// dotenv.config()

// REGISTER 
// localhost:5000/api/auth/register
router.post("/register", async (req,res) => {
    const newUser = new User ({
        username : req.body.username,
        email : req.body.email,
        //password : req.body.password,
        password: CryptoJS.AES.encrypt(req.body.password, process.env.secret)
        .toString()
    })

    try{
        const savedUser = await newUser.save()
        res.status(201).json(savedUser)
    } catch(err){
        console.log(err)
        res.status(500).json(err)
    }
});


// LOGIN
// localhost:5000/api/auth/login
router.post("/login", async (req,res) => {
    try{
        const user = await User.findOne({username:req.body.username})
        !user && res.status(401).json("Wrong username or password")


         
        const hashedPassword = CryptoJS.AES.decrypt(
            user.password,
            process.env.secret
            )
            const OriginalPassword = hashedPassword.toString(CryptoJS.enc.Utf8)
            OriginalPassword !== req.body.password && 
                res.status(401).json("Wrong username or password");
                // hide password key to the _doc

                // json web token expires in 3days and you have to login again
                const accessToken = jwt.sign({
                    id:user._id, 
                    isAdmin : user.isAdmin
                },
                process.env.jwt_secret,
                {expiresIn:"3d"}
                )
               
                const {password, ...others} = user._doc

                res.status(200).json({...others, accessToken})
                
    } catch(err){
        res.status(500).json(err)
    }
})


  



module.exports = router