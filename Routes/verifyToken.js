const jwt = require("jsonwebtoken")

const verifyToken = (req,res, next) => {
    const authHeader = req.headers.token
    if(authHeader){
        const token = authHeader.split(" ")[1];
        jwt.verify(token, process.env.jwt_secret, (err, user) => {
            if(err) return res.status(401).json("Token is not Valid")
            req.user = user
            next() // goes to our router
        })
    }else{
        return res.status(401).json("You are not authenticated")
    }
}

// update
const verifyTokenAndAuthorization = (req,res,next) =>{
    verifyToken(req, res, () => {
        if(req.user.id === req.params.id || req.user.isAdmin){
            next()
        } else {
            res.status(403).json("You're not allowed to do that")
        }
    })
}

const verifyTokenAndAdmin = (req,res,next) =>{
    verifyToken(req, res, () => {
        if(req.user.isAdmin){
            next()
        } else {

            res.status(403).json("You're not an admin")
        }
    })
}


module.exports = {verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin}
// select headers in postman
// bearer (json webtoken)`