
const {verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin} = require("./verifyToken")
const router = require("express").Router()
const User = require("../models/user")

// test
/* 
router.get("/usertest", (req,res) => {
    res.send("user test is successful")
})

router.post("/userposttest", (req, res) => {
    const username = req.body.username
    console.log(username)
    res.send("your username is :" + username)
})
*/

// update username // admin only // 
// localhost:5000/api/users/:id
router.put("/:id", verifyTokenAndAuthorization, async (req,res) => {
    if(req.body.password){
        req.body.password = CryptoJS.AES.encrypt(req.body.password, process.env.secret)
        .toString() 
    }
    try{
        const updatedUser = await User.findByIdAndUpdate(
        req.params.id,  
        {
        $set: req.body
        },
        {new:true}
        );
        res.status(200).json(updatedUser)
    } catch (err) {
        res.status(500).json(err)
    }
})  

// make admin 
router.put("/makeMeAdmin/:id", verifyTokenAndAdmin, async (req,res) => {
    if(req.body.password){
        req.body.password = CryptoJS.AES.encrypt(req.body.password, process.env.secret)
        .toString() 
    }
    try{
        const updatedStatus = await User.findByIdAndUpdate(
        req.params.id,  
        {
        $set: req.body.isAdmin
        },
        {new:true}
        );
        res.status(200).json(updatedStatus)
    } catch (err) {
        res.status(500).json(err)
    }
})  

// delete

router.delete("/:id", verifyTokenAndAuthorization, async (req,res) => {
    try { 
       await User.findByIdAndDelete(req.params.id)
       res.status(200).json("User has been deleted")
    }catch(err){
        res.status(500).json(err)
    }
}) 


// get user || as admin 
router.get("/find/:id", verifyTokenAndAdmin, async (req,res) => {
    try { 
       const user = await User.findById(req.params.id)
       const {password, ...others} = user._doc
       res.status(200).json(others)
    }catch(err){
        res.status(500).json(err)
    }
}) 

// get all users
router.get("/findAll", verifyTokenAndAdmin, async (req,res) => {
    const query = req.query.new
    try { 
       const users = query 
        ? await User.find().sort({ _id: -1 }).limit(5) // pull new users || /users?new=true
        : await User.find()
       res.status(200).json(users)
    }catch(err){
        res.status(500).json(err)
    }
}) 

// get user stats
// show the month and number of accounts created

router.get("/stats", verifyTokenAndAdmin, async (req, res) => {
    const date = new Date()
    const lastYear = new Date(date.setFullYear(date.getFullYear() - 1))
    try{
        
        const data = await User.aggregate([
            
         {$match : {createdAt : {$gte: lastYear}}},
         {
            $project : {
                month : {$month : "$createdAt"}
            }
         },
         {
             $group : {
                 _id: "$month", // show month 
                 total : {$sum : 1} // show how many accounts created this month 
             }
         }
        ])
        res.status(200).json(data)
    }catch(err){
        res.status(500).json(err)
    }
})

module.exports = router