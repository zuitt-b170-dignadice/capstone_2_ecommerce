
const {verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin} = require("./verifyToken")
const router = require("express").Router()
const Product = require("../models/product.js")



// CREATE

router.post("/", verifyTokenAndAuthorization, async (req, res) => {
    const newProduct = new Product(req.body)

    try{
        const savedProduct = await newProduct.save();
        res.status(200).json(savedProduct)
    }catch(err){
        res.status(500).json(err)
    }
})


// UPDATE

router.put("/updateProduct/:id", verifyTokenAndAdmin, async (req, res) => {
    try{
        const updatedProduct = await Product.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body,
            },
            {new: true}
        )
        res.status(200).json(updatedProduct)
    }catch(err){
        res.status(500).json(err)
    }
})

// ARCHIVE   

router.put("/archiveProduct/:id", verifyTokenAndAdmin, async (req, res) => {
    try{
        const updatedStatus = await Product.findByIdAndUpdate(
            {_id: req.params.id},{"$set":{"isActive":false}}
        )
        res.status(200).json(updatedStatus)
    }catch(err){
        res.status(500).json(err)
    }
})

// UNARCHIVE PRODUCT 

router.put("/unarchiveProduct/:id", verifyTokenAndAdmin, async (req, res) => {
    try{
        const updatedStatus = await Product.findByIdAndUpdate(
            {_id: req.params.id},{"$set":{"isActive":true}}
        )
        res.status(200).json(updatedStatus)
    }catch(err){
        res.status(500).json(err)
    }
})

// DELETE 
router.delete("/:id", verifyTokenAndAdmin, async (req,res) => {
    try { 
       await Product.findByIdAndDelete(req.params.id)
       res.status(200).json("Product has been deleted")
    }catch(err){
        res.status(500).json(err)
    }
}) 

// GET Product 

router.get("/find/:id", async (req,res) => {
    try { 
       const product = await Product.findById(req.params.id)
       res.status(200).json(product)
    }catch(err){
        res.status(500).json(err)
    }
}) 

// GET All active Product 

router.get("/activeProducts", async (req,res) => {
    const qActive = req.query.isActive

    try { 
        let products
        if(qActive){
            products = await Product.find({isActive : {$in : [qActive]}})
        } // ?isActive=true
     

       res.status(200).json(products)
    }catch(err){
        res.status(500).json(err)
    }
}) 


// Get all Products 
router.get("/", async (req,res) => {
    const qNew = req.query.new
    const qCategory = req.query.category
    try { 
        let products;
        if(qNew){
            products = await Product.find().sort({createdAt: -1}).limit(5)
        } else if (qCategory){
            products = await Product.find({category:{$in : [qCategory] // /products?category=tshirt
        }
        })
    } else {
        products = await Product.find()
    }
       
       res.status(200).json(products)
    }catch(err){
        res.status(500).json(err)
    }
}) 



module.exports = router